package crudconsola;

import java.sql.*;//para no hacer muchas importaciones 

public class Conexion {

    private static String user = "root";
    private static String password = "";
    private static String host = "localhost";
    private static String db = "empresa";
    private static String url = "jdbc:mysql://"+host+"/"+db;
    private static String driver = "com.mysql.jdbc.Driver";

    private static Connection conexion;

    public Conexion() {
        try {
            Class.forName(driver);//levanto el driver
            conexion = DriverManager.getConnection(url,user,password);//establesco la conexion
            System.out.println("conexion estableciad");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("error al conectar");
        }
    }
    public Connection getConnection(){
        return conexion;//devuelve conexion el constructor al instanciar Conexion
    }

}
