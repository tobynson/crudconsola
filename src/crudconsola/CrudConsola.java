package crudconsola;

import java.sql.*;//para no hacer muchas importaciones 
import java.util.Scanner;

public class CrudConsola {
    private static Scanner leer=new Scanner(System.in);
    private static String nombre="";
    private static String departamento="";
    private static int opcion;
    private static Conexion conexion;

    public static void main(String[] args) {
        do { 
            System.out.println("elije una opcion:");
            System.out.println("1. insertar");
            System.out.println("2. editar");
            System.out.println("3. eliminar");
            System.out.println("4. mostrar");
            System.out.println("5. salir");
            
            opcion=leer.nextInt();
            switch (opcion) {
                case 1:
                    Insertar();
                    break;
                case 2:
                    Editar();
                    break;
                case 3:
                    Eliminar();
                    break;
                case 4:
                    Mostrar();
                    break;
                case 5:
                    System.out.println("saliendo...");
                    System.exit(0);
                default:
                    System.out.println("escribe una opcion del 1 al 5");;
            }
            
        } while (opcion!=5);
        
    }
    public static void Insertar(){
        //cargamos la conexion
        conexion=new Conexion();
        Connection cn=conexion.getConnection();
        Statement st; //procesa las consultas sql
        
        System.out.println("ingrese el nombre del empleado");
        nombre=leer.next();
        System.out.println("ingrese el departamento del empleado");
        departamento=leer.next();        
        String sql="insert into empleados(nombre,departamento) values ('"+nombre+"','"+departamento+"')";
        
        try {
            st=cn.createStatement();//establecemos la comunicacion entre java y la bd
            st.executeUpdate(sql);//ejecuta el comando y ctualiza la tabla
            //cerramos las coneciones
            cn.close();
            st.close();
            System.out.println("se inserto correctamente los datos...!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    public static void Editar(){
        //cargamos la conexion
        conexion=new Conexion();
        Connection cn=conexion.getConnection();
        Statement st; 
        //ingresamos el id del registro a modificar
        System.out.println("ingrese el id del registro a modificar");
        int id=leer.nextInt();
        //pedimos los demas datos a modificar
        System.out.println("ingrese el nuevo nombre del empleado");
        nombre=leer.next();
        System.out.println("ingrese el nuevo departamento del empleado");
        departamento=leer.next();
        //cremos la sentencia sql
        String sql="update empleados set nombre='"+nombre+"',departamento='"+departamento+"' where id='"+id+"' ";
        //ejecutamos la sentencia
        try {
            st=cn.createStatement();
            int confirmar=st.executeUpdate(sql);
            if (confirmar==1) {
                System.out.println("registro actualizado correctamente...!!");
            }else{
                System.out.println("error al actualizar el registro");
            }
            //cerramos las coneciones
            cn.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void Eliminar(){
        //cargamos la conexion
        conexion=new Conexion();
        Connection cn=conexion.getConnection();
        Statement st; 
        //ingresamos el id del registro a eliminar
        System.out.println("ingrese el id del registro a eliminar");
        int id=leer.nextInt();
        //cremos la sentencia sql
        String sql="delete from empleados where id="+id;
        //ejecutamos la sentencia
        try {
            st=cn.createStatement();
            int confirmar=st.executeUpdate(sql);
            if (confirmar==1) {
                System.out.println("registro eliminado correctamente...!!");
            }else{
                System.out.println("error al eliminar el registro");
            }
            //cerramos las coneciones
            cn.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void Mostrar(){
        //cargamos la conexion
        conexion=new Conexion();
        Connection cn=conexion.getConnection();
        Statement st; 
        ResultSet rs;
        //cremos la sentencia sql
        String sql="select * from empleados";
        //ejecutamos la sentencia
        try {
            st=cn.createStatement();
            rs=st.executeQuery(sql);
            //mostramos los datos
            while (rs.next()) {                
                System.out.println("Id:"+rs.getInt(1));
                System.out.println("Nombre:"+rs.getString(2));
                System.out.println("Departamento:"+rs.getString(3));
            }
            //cerramos las coneciones
            cn.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
